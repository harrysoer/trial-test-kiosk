import { TestBed, inject } from '@angular/core/testing';

import { KioskService } from './kiosk.service';

describe('KioskService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [KioskService]
    });
  });

  it('should be created', inject([KioskService], (service: KioskService) => {
    expect(service).toBeTruthy();
  }));
});
