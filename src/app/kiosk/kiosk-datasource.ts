export interface KioskItem {
    name: string;
    info: string;
    id: number;
}

export const KIOSK_MOCK_DATA: KioskItem[] = [{
    id: 1,
    info: "In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.",
    name: "Bonneville"
}, {
    id: 2,
    info: "Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.",
    name: "Crossfire Roadster"
}, {
    id: 3,
    info: "In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.",
    name: "C-Class"
}, {
    id: 4,
    info: "Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.",
    name: "X6 M"
}, {
    id: 5,
    info: "Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.",
    name: "Mazda5"
}, {
    id: 6,
    info: "Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.",
    name: "Savana 1500"
}, {
    id: 7,
    info: "Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.",
    name: "S-Class"
}, {
    id: 8,
    info: "Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.",
    name: "Escalade ESV"
}, {
    id: 9,
    info: "In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.",
    name: "Magnum"
}, {
    id: 10,
    info: "Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.",
    name: "Fusion"
}]


