import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventPerKioskComponent } from './event-per-kiosk.component';

describe('EventPerKioskComponent', () => {
  let component: EventPerKioskComponent;
  let fixture: ComponentFixture<EventPerKioskComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventPerKioskComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventPerKioskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
